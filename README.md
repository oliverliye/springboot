#### 本项目涉及到的代码、配置文件、maven依赖都有注释说明，简单易懂，不走弯路。
#### 目录结构说明：
##### src/main/java
    com.hkb.springboot
        aspect———————————————里面是web请求日志（AOP统一处理）
        bean—————————————————里面是controller统一返回的bean
        config———————————————里面是配置
        dao——————————————————里面是数据库操作（JPA）
        entity———————————————里面是数据库实体
        exception————————————里面是自定义异常
        filter———————————————里面是过滤器
        handler——————————————里面是全局异常处理
        interceptor——————————里面是拦截器
        listener—————————————里面是监听器
        service——————————————里面是接口及实现类（有事务，有部分方法有Redis缓存）
        util—————————————————里面是工具类
        web——————————————————里面是controller
    StartApplication.java————启动类
##### src/main/resources
        static————————————————静态资源文件夹
        application-dev.yml———开发环境配置
        application-prod.yml——生产环境配置
        application-test.yml——测试环境配置
        application.yml———————多环境配置
        banner.txt————————————banner彩蛋
        logback-spring.xml————logback日志
##### src/main/webapp
        upload———————————————上传文件的文件夹
        WEB-INF
            jsp
                index.jsp————上传单个和多个文件的jsp
##### src/main/test
    com.hkb.springboot
        service
            BaseTest.java—————————————————测试基类
            StudentServiceTest.java———————接口测试
        web
            StudentControllerTest.java————controller测试