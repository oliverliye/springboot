/**
 * Created on 2018年3月12日 下午8:44:25
 */
package com.hkb.springboot.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学生实体类 . <br>
 * 
 * @author hkb <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "student")
public class StudentEntity implements Serializable {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = -3967265553147264980L;

    /**
     * 学生id
     */
    @Id
    @GeneratedValue
    @ApiModelProperty(value = "学生id")
    private Integer id;

    /**
     * 学生姓名
     */
    @ApiModelProperty(value = "学生姓名")
    private String name;

    /**
     * 学生年龄
     */
    @ApiModelProperty(value = "学生年龄")
    private Integer age;

}