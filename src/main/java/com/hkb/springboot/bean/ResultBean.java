/**
 * Created on 2017年12月1日 上午9:18:03 <br>
 */
package com.hkb.springboot.bean;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 返回对象包装类(带泛型) .<br>
 * 
 * @author hkb <br>
 */
@Data
@ApiModel(description = "返回响应数据")
public class ResultBean<T> implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 4162884113813357846L;

    /**
     * 成功
     */
    public static final int SUCCESS = 0;

    /**
     * 失败
     */
    public static final int CHECK_FAIL = 1;

    /**
     * 未知异常
     */
    public static final int UNKNOWN_EXCEPTION = -99;

    /**
     * 返回的信息(主要出错的时候使用)
     */
    @ApiModelProperty(value = "响应信息")
    private String msg;

    /**
     * 接口返回码
     */
    @ApiModelProperty(value = "响应编码")
    private int code;

    /**
     * 返回的数据
     */
    @ApiModelProperty(value = "响应数据")
    private T data;

    /**
     * 无参构造函数
     */
    public ResultBean() {
        super();
    }

    /**
     * 有参构造函数
     * 
     * @param data
     */
    public ResultBean(T data) {
        super();
        init(data);
    }

    /**
     * 初始化
     */
    private void init(T data) {
        this.msg = "success";
        this.code = SUCCESS;
        this.data = data;
    }

}