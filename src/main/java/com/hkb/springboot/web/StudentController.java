/**
 * Created on 2018年3月11日 下午9:07:20
 */
package com.hkb.springboot.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hkb.springboot.bean.ResultBean;
import com.hkb.springboot.entity.StudentEntity;
import com.hkb.springboot.service.StudentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 学生信息controller . <br>
 * 
 * @author hkb <br>
 */
@Api(tags = { "学生信息接口" })
@RestController
public class StudentController {

    /**
     * 学生信息接口
     */
    @Autowired
    private StudentService studentService;
    // private final StudentService studentService;
    //
    // /**
    // * 另一种注入方式
    // *
    // * @param studentService
    // */
    // @Autowired
    // public StudentController(StudentService studentService) {
    // super();
    // this.studentService = studentService;
    // }

    /**
     * 添加学生信息
     * 
     * @param studentEntity
     * @return
     */
    @ApiOperation(value = "添加学生信息", notes = "根据StudentEntity对象创建学生信息")
    @ApiImplicitParam(name = "student", value = "StudentEntity对象", dataType = "StudentEntity")
    @PostMapping("/student")
    public ResultBean<Integer> addStudent(@RequestBody StudentEntity student) {
        return new ResultBean<Integer>(studentService.addStudent(student));
    }

    /**
     * 修改学生信息
     * 
     * @param studentEntity
     * @return
     */
    @ApiOperation(value = "修改学生信息", notes = "根据StudentEntity对象来修改学生信息")
    @ApiImplicitParam(name = "student", value = "StudentEntity对象", dataType = "StudentEntity")
    @PutMapping("/student")
    public ResultBean<Integer> updateStudent(@RequestBody StudentEntity student) {
        return new ResultBean<Integer>(studentService.updateStudent(student));
    }

    /**
     * 删除学生信息
     * 
     * @param studentEntity
     * @return
     */
    @ApiOperation(value = "删除学生信息", notes = "根据url的id来指定删除学生信息")
    @ApiImplicitParam(name = "id", value = "学生id", required = true, paramType = "path", dataType = "int")
    @DeleteMapping("/student/{id}")
    public ResultBean<Boolean> deleteStudentById(@PathVariable("id") Integer id) {
        return new ResultBean<Boolean>(studentService.deleteStudentById(id));
    }

    /**
     * 根据id查询学生信息
     * 
     * @param id
     * @return
     */
    @ApiOperation(value = "查询学生信息", notes = "根据url的id查询学生信息")
    @ApiImplicitParam(name = "id", value = "学生id", required = true, paramType = "path", dataType = "int")
    @GetMapping("/student/{id}")
    public ResultBean<StudentEntity> findStudentById(@PathVariable("id") Integer id) {
        return new ResultBean<StudentEntity>(studentService.findStudentById(id));
    }

    /**
     * 根据年龄查询学生信息
     * 
     * @param age
     * @return
     */
    @ApiOperation(value = "根据年龄查询学生信息", notes = "根据url的age查询学生信息")
    @ApiImplicitParam(name = "age", value = "学生年龄", required = true, paramType = "path", dataType = "int")
    @GetMapping("/student/age/{age}")
    public ResultBean<List<StudentEntity>> findStudentByAge(@PathVariable("age") Integer age) {
        return new ResultBean<List<StudentEntity>>(studentService.findStudentByAge(age));
    }

    /**
     * 查询所有学生信息
     * 
     * @return
     */
    @ApiOperation(value = "查询所有学生信息")
    @GetMapping("/student")
    public ResultBean<List<StudentEntity>> findAllStudent() {
        return new ResultBean<List<StudentEntity>>(studentService.findAllStudent());
    }

}
