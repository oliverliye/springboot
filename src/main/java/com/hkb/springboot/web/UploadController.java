/**
 * Created on 2018年3月23日 上午9:47:58
 */
package com.hkb.springboot.web;

import java.io.File;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 文件上传controller . <br>
 * 
 * @author hkb <br>
 */
@ApiIgnore
@RestController
public class UploadController {

    /**
     * 跳转到index.jsp
     * 
     * @return
     */
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("index");
        return view;
    }

    /**
     * 单个文件上传
     * 
     * @param file
     *            前台上传的文件对象
     * @param session
     * @return
     * @throws Exception
     */
    @PostMapping("/upload")
    public String upload(HttpSession session, MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return "fail";
        }
        // 上传目录地址
        String uploadDir = session.getServletContext().getRealPath("/upload");
        // 判断目录是否存在,不存在则创建
        File dir = new File(uploadDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        // 调用上传方法
        uploadRealize(uploadDir, file);
        return "success";
    }

    /**
     * 多个文件上传
     * 
     * @param files
     *            上传文件集合
     * @param session
     * @return
     * @throws Exception
     */
    @PostMapping("/uploads")
    public String uploads(HttpSession session, MultipartFile[] files) throws Exception {
        int filesLen = files.length;
        if (filesLen < 0) {
            return "fail";
        }
        // 上传目录地址
        String uploadDir = session.getServletContext().getRealPath("/upload");
        // 判断目录是否存在,不存在则创建
        File dir = new File(uploadDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                // 调用上传方法
                uploadRealize(uploadDir, file);
            }
        }
        return "success";
    }

    /**
     * 上传实现
     * 
     * @param uploadDir
     *            上传文件目录
     * @param file
     *            上传对象
     * @throws Exception
     */
    private void uploadRealize(String uploadDir, MultipartFile file) throws Exception {
        // 上传文件名
        String fileName = file.getOriginalFilename();
        // 文件后缀名
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        // 上传文件名
        String filename = UUID.randomUUID().toString().replace("-", "") + suffix;
        // 服务器端保存的文件对象
        File serverFile = new File(uploadDir + File.separator + filename);
        // 将上传的文件写入到服务器端文件内
        file.transferTo(serverFile);
    }

}
