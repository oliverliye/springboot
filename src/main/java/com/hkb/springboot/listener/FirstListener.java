/**
 * Created on 2018年3月30日 上午10:04:09
 */
package com.hkb.springboot.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 监听器配置,需要主类加入@ServletComponentScan . <br>
 * 
 * @author hkb <br>
 */
@WebListener
public class FirstListener implements ServletContextListener {

    /**
     * 日志对象
     */
    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("FirstListener开始......");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("FirstListener销毁......");
    }

}
