/**
 * Created on 2018年3月20日 上午10:43:56
 */
package com.hkb.springboot.service;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试基类 . <br>
 * SpringBootTest.WebEnvironment.RANDOM_POR 表示使用随机端口号
 * 
 * @author hkb <br>
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Ignore
public class BaseTest {

}
