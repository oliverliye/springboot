/**
 * Created on 2018年3月20日 下午2:25:50
 */
package com.hkb.springboot.web;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.hkb.springboot.entity.StudentEntity;
import com.hkb.springboot.service.StudentService;

/**
 * 学生信息controller测试. <br>
 * 
 * @author hkb <br>
 */
@RunWith(SpringRunner.class)
@WebMvcTest(StudentController.class)
public class StudentControllerTest {

    /**
     * mvc测试对象-被测类StudentController
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * 学生信息接口-模拟对象
     */
    @MockBean
    private StudentService studentService;

    /**
     * 学生实体
     */
    private StudentEntity student;

    /**
     * 学生id
     */
    private Integer id = -99;

    /**
     * 学生姓名
     */
    private String studentName = "test";

    /**
     * 学生年龄
     */
    private Integer studentAge = 20;

    /**
     * 所有测试方法执行之前执行该方法
     */
    @Before
    public void before() {
        // 获取mockmvc对象实例
        // mockMvc =
        // MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        student = new StudentEntity(id, studentName, studentAge);
        // 设置模拟对象的返回预期值
        Mockito.when(studentService.addStudent(student)).thenReturn(id);
        Mockito.when(studentService.updateStudent(student)).thenReturn(id);
        Mockito.when(studentService.deleteStudentById(id)).thenReturn(true);
        Mockito.when(studentService.findStudentById(id)).thenReturn(student);
        Mockito.when(studentService.findStudentByAge(studentAge)).thenReturn(Lists.newArrayList(student));
        Mockito.when(studentService.findAllStudent()).thenReturn(Lists.newArrayList(student));
    }

    /**
     * 测试添加
     *
     * @throws Exception
     */
    @Test
    public void addStudentTest() throws Exception {
        StudentEntity student = new StudentEntity();
        student.setId(id);
        student.setName(studentName);
        student.setAge(studentAge);
        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.post("/student").content(JSON.toJSONString(student))).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertThat(status, Matchers.is(200));
    }

    /**
     * 测试修改
     *
     * @throws Exception
     */
    @Test
    public void updateStudentTest() throws Exception {
        StudentEntity student = new StudentEntity();
        student.setId(id);
        student.setName(studentName);
        student.setAge(studentAge);
        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.put("/student").content(JSON.toJSONString(student))).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertThat(status, Matchers.is(200));
    }

    /**
     * 测试删除
     *
     * @throws Exception
     */
    @Test
    public void deleteStudentByIdTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/student/" + id)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertThat(status, Matchers.is(200));
    }

    /**
     * 测试根据Id查询
     *
     * @throws Exception
     */
    @Test
    public void findStudentByIdTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/student/" + id)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertThat(status, Matchers.is(200));
    }

    /**
     * 测试根据年龄查询
     *
     * @throws Exception
     */
    @Test
    public void findStudentByAgeTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/student/age/" + id)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertThat(status, Matchers.is(200));
    }

    /**
     * 测试查询所有
     *
     * @throws Exception
     */
    @Test
    public void findAllStudentTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/student")).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertThat(status, Matchers.is(200));
    }

}
